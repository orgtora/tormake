# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

include_guard(GLOBAL)

find_program(TORMAKE_FORMAT tormake-format REQUIRED NO_DEFAULT_PATH
  PATHS ${TORMAKE_BINARY_PATH_ARRAY} DOC "tormake-format binary")

find_program(TORMAKE_CLANG_FORMAT clang-format REQUIRED NO_DEFAULT_PATH
  PATHS ${TORMAKE_BINARY_PATH_ARRAY} DOC "clang-format binary")


add_test(
  NAME tormake.format
  WORKING_DIRECTORY {PROJECT_SOURCE_DIR}
  COMMAND
  ${TORMAKE_FORMAT} --clang-format ${TORMAKE_CLANG_FORMAT} --source-dir ${PROJECT_SOURCE_DIR} --check
  )
