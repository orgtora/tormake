# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include(CMakePushCheckState)
cmake_push_check_state()

set(CMAKE_REQUIRED_QUIET ${tormake_FIND_QUIETLY})
set(want_components ${tormake_FIND_COMPONENTS})

set(extra_components ${want_components})

list(
  REMOVE_ITEM
  extra_components
  clang-tidy
  )

foreach(component IN LISTS extra_components)
  message(FATAL_ERROR "Invalid find_package component for tormake: ${component}")
endforeach()

include(${CMAKE_CURRENT_LIST_DIR}/common.cmake)

foreach(component IN LISTS want_components)
  include(${CMAKE_CURRENT_LIST_DIR}/${component}.cmake)
endforeach()

cmake_pop_check_state()
