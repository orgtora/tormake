# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

target_compile_options(tora::coverage INTERFACE
  $<$<COMPILE_LANGUAGE:C>:-fprofile-instr-generate>
  $<$<COMPILE_LANGUAGE:C>:-fcoverage-mapping>
  $<$<COMPILE_LANGUAGE:CXX>:-fprofile-instr-generate>
  $<$<COMPILE_LANGUAGE:CXX>:-fcoverage-mapping>)

target_link_options(tora::coverage INTERFACE
  -fprofile-instr-generate
  -fcoverage-mapping)



