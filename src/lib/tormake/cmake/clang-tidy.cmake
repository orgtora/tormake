# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)


set(TORMAKE_CLANG_TIDY_PASSTHROUGH "" CACHE STRING "Passthrough arguments for clang-tidy")

find_program(TORMAKE_RUN_CLANG_TIDY run-clang-tidy REQUIRED NO_DEFAULT_PATH
  PATHS ${TORMAKE_BINARY_PATH_ARRAY} DOC "run-clang-tidy executable")

find_program(TORMAKE_CLANG_TIDY clang-tidy REQUIRED NO_DEFAULT_PATH
  PATHS ${TORMAKE_BINARY_PATH_ARRAY} DOC "clang-tidy executable")

add_test(
  NAME tormake.clang-tidy
  WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
  COMMAND
    ${TORMAKE_RUN_CLANG_TIDY} -clang-tidy-binary ${TORMAKE_CLANG_TIDY}
    -extra-arg="${TORMAKE_CLANG_TIDY_PASSTHROUGH}" -p ${PROJECT_BINARY_DIR}
  DEPENDS ${PROJECT_BINARY_DIR}/compile_commands.json
  )



