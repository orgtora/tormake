# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
include_guard(GLOBAL)
message(DEBUG "Enabling tormake::coverage")

set(TORMAKE_COVERAGE_OUTDIR ${PROJECT_BINARY_DIR}/coverage
  CACHE PATH "Path to Store Output of Coverage Data")

# Interface Library to Collect Compiler Options for Coverage
add_library(tormake::coverage INTERFACE IMPORTED)

add_custom_target(tora.coverage)
if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  include(${CMAKE_CURRENT_LIST_DIR}/coverage/clang.cmake)
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  include(${CMAKE_CURRENT_LIST_DIR}/coverage/gcc.cmake)
else()
  include(${CMAKE_CURRENT_LIST_DIR}/coverage/dummy.cmake)
endif()
message(DEBUG "Enabling tormake::coverage - Done")
